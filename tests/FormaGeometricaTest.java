import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FormaGeometricaTest {
	public float altura;
	public float base;
	public String tipo;
	public FormaGeometrica forma;
	
	@BeforeEach
	public void beforeTests(){
		// Preparação
		altura = 5.0f;
		base = 10.0f;
		tipo = "Retângulo";
		forma = new FormaGeometrica(base, altura);
	}
	
	@Test
	public void testaConstrutor() {

		// Ação
		forma = new FormaGeometrica(base, altura);
		
		// Verificação
		assertEquals(base,forma.getBase());
		assertEquals(altura,forma.getAltura());
		assertEquals(this.tipo,this.forma.getTipo());		
		
	}
	 
	@Test
	public void testaCalculaArea() {
		float area = 50.0f;
		
		assertEquals(area, forma.calculaArea(),10e-5);		
		
		forma.setBase(3.5f);
		forma.setAltura(4.8f);
		assertEquals(16.8f, forma.calculaArea(),10e-5);
	}
	
	@Test
	public void testaCalculaPerimetro() {
		float perimetro = 30.0f;
		
		assertEquals(perimetro,forma.calculaPerimetro(),10e-5);
	}
	
	@AfterEach
	void afterTests() {
		forma = null;
	}
}
